const SET_INFO_PAGE = 'SET_INFO_PAGE';
const SET_MENU_TYPE = 'SET_MENU_TYPE';

const initialState = {
  isOpenMenu: true,
  infoPage: {
    id: 1,
    active: true
  }
};

export default function global(state = initialState, action = {}) {
  switch (action.type) {
    case SET_INFO_PAGE:
      return {
        ...state,
        infoPage: action.infoPage
      };
    case SET_MENU_TYPE:
      return {
        ...state,
        isOpenMenu: action.isOpenMenu
      };
    default:
      return state;
  }
}

export function setInfoPage(infoPage) {
  return { type: SET_INFO_PAGE, infoPage };
}

export function setMenuType(isOpenMenu) {
  return { type: SET_MENU_TYPE, isOpenMenu };
}
