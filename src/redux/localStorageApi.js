export function loadStorage(nameLocalStorage) {
  try {
    const appStore = localStorage.getItem(nameLocalStorage);
    if (appStore === null) {
      return undefined;
    }
    return JSON.parse(appStore);
  } catch (err) {
    console.log(err);
    return undefined;
  }
}

export function saveLocalStorage(nameLocalStorage, saveItem) {
  try {
    const saveItemString = JSON.stringify(saveItem);
    localStorage.setItem(nameLocalStorage, saveItemString);
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
}

export function deleteLocalStorage(nameLocalStorage) {
  try {
    localStorage.removeItem(nameLocalStorage);
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
}
