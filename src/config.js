require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  app: {
    title: 'Admin Vechorka',
    description: 'Admin Vechorka',
    head: {
      titleTemplate: 'Admin Vechorka: %s',
      meta: [
        {name: 'description', content: 'Admin Vechorka'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Admin Vechorka'},
        {property: 'og:image', content: ''},
        {property: 'og:locale', content: 'ru_RU'},
        {property: 'og:title', content: 'Admin Vechorka'},
        {property: 'og:description', content: 'Admin Vechorka'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@nightfury2'},
        {property: 'og:creator', content: '@nightfury2'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },

}, environment);
