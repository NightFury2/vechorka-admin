import React from 'react';

import NewspaperMin from '../../components/Newspaper/NewspaperMin/NewspaperMin';
import NewsMini from '../../components/News/NewsMini/NewsMini';

import Divider from 'material-ui/Divider';

import {setTitle} from '../../redux/modules/appBar';
import {setInfoPage} from '../../redux/modules/global';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';

@connect(
  state => ({
    title: state.appBar.title,
    infoPage: state.global.infoPage
  }),
  {setTitle, setInfoPage, pushState: push})
export default class Home extends React.Component {
  static propTypes = {
    // redux
    pushState: React.PropTypes.func.isRequired,
    // appBar
    setTitle: React.PropTypes.func,
    // global
    infoPage: React.PropTypes.object.isRequired,
    setInfoPage: React.PropTypes.func.isRequired
  };
  componentDidMount() {
    this.props.setTitle('Главная');
    this.props.setInfoPage({id: 1});
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.infoPage.id !== nextProps.infoPage.id) {
      this.props.setInfoPage({id: 1});
    }
  }
  render() {
    return (
      <div className="col s12" style={{marginTop: '20px'}}>
        <div className="row">
          <div className="col s12">
            <NewspaperMin pushState={this.props.pushState}/>
          </div>
          <Divider/>
          <div className="col s12">
            <NewsMini pushState={this.props.pushState}/>
          </div>
        </div>
      </div>
    );
  }
}
