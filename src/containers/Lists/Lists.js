import React from 'react';

import {setTitle} from '../../redux/modules/appBar';
import {setInfoPage} from '../../redux/modules/global';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';

@connect(
  state => ({
    title: state.appBar.title,
    infoPage: state.global.infoPage
  }),
  {setTitle, setInfoPage, pushState: push})
export default class Lists extends React.Component {
  static propTypes = {
    // appBar
    setTitle: React.PropTypes.func,
    // global
    infoPage: React.PropTypes.object.isRequired,
    setInfoPage: React.PropTypes.func.isRequired
  };
  componentDidMount() {
    this.props.setTitle('Списки');
    this.props.setInfoPage({id: 6});
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.infoPage.id !== nextProps.infoPage.id) {
      this.props.setInfoPage({id: 6});
    }
  }
  render() {
    return (
      <div className="col s12">
        <div className="container">
          <div className="row">
            <h1>hello Lists</h1>
          </div>
        </div>
      </div>
    );
  }
}
