import React from 'react';

import {setTitle} from '../../redux/modules/appBar';
import {setInfoPage} from '../../redux/modules/global';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';

@connect(
  state => ({
    title: state.appBar.title,
    infoPage: state.global.infoPage
  }),
  {setTitle, setInfoPage, pushState: push})
export default class Newspaper extends React.Component {
  static propTypes = {
    // appBar
    setTitle: React.PropTypes.func,
    // global
    infoPage: React.PropTypes.object.isRequired,
    setInfoPage: React.PropTypes.func.isRequired
  };
  componentDidMount() {
    this.props.setTitle('Газета');
    this.props.setInfoPage({id: 4});
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.infoPage.id !== nextProps.infoPage.id) {
      this.props.setInfoPage({id: 4});
    }
  }
  render() {
    return (
      <div className="col s12">
        <div className="container">
          <div className="row">
            <h1>hello Newspaper</h1>
          </div>
        </div>
      </div>
    );
  }
}
