import React from 'react';
import Helmet from 'react-helmet';
import config from '../../config';

import MenuContent from '../../components/MenuContent/MenuContent';
import RightMenuNavbar from '../../components/RightMenuNavbar/RightMenuNavbar';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import spacing from 'material-ui/styles/spacing';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import LinearProgress from 'material-ui/LinearProgress';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

import NavigationCloseIcon from 'material-ui/svg-icons/navigation/close';

import {
  cyan500,
  pinkA200,
  grey100, grey300, grey400, grey500, grey800,
  white, darkBlack, fullBlack, teal500
} from 'material-ui/styles/colors';
import {fade} from 'material-ui/utils/colorManipulator';

import {setTitle} from '../../redux/modules/appBar';
import {setInfoPage, setMenuType} from '../../redux/modules/global';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';

const muiTheme = {
  spacing: spacing,
  fontFamily: 'Roboto, sanc-serif',
  palette: {
    primary1Color: teal500,
    primary2Color: white,
    primary3Color: grey400,
    accent1Color: pinkA200,
    accent2Color: grey100,
    accent3Color: grey500,
    textColor: fullBlack,
    secondaryTextColor: fade(darkBlack, 0.54),
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey300,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: cyan500,
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  }
};

@connect(
  state => ({
    title: state.appBar.title,
    infoPage: state.global.infoPage,
    isOpenMenu: state.global.isOpenMenu
  }),
  {setTitle, setInfoPage, setMenuType, pushState: push})
export default class App extends React.Component {
  static propTypes = {
    children: React.PropTypes.object.isRequired,
    // react-router-redux
    pushState: React.PropTypes.func.isRequired,
    // appBar
    title: React.PropTypes.string,
    setTitle: React.PropTypes.func,
    // global
    infoPage: React.PropTypes.object.isRequired,
    isOpenMenu: React.PropTypes.bool.isRequired,
    setInfoPage: React.PropTypes.func.isRequired,
    setMenuType: React.PropTypes.func.isRequired
  };
  state = {
    openMenu: false
  };
  componentDidMount() {
    this.props.setTitle('Главная');
  }
  menuOpen = () => {
    this.setState({openMenu: true});
  };
  menuClose = () => {
    this.setState({openMenu: false});
  };
  render() {
    const {openMenu} = this.state;
    const styles = require('./App.scss');
    const classContent = this.props.isOpenMenu ? '' : styles.contentMax;
    return (
       <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
          <div>
            <div className={styles.lineProgress}>
              <LinearProgress color={teal500} mode="indeterminate"/>
            </div>
            <div className={'row ' + styles.app}>
              <AppBar
                style={{background: white}}
                titleStyle={{color: 'black'}}
                title={<div><span className="hide-on-small-only">Вечерний ставрополь: </span><span>{this.props.title || 'Загрузка'}</span></div>}
                iconElementRight={<RightMenuNavbar/>}
                iconElementLeft={<IconButton className="hide-on-med-and-up" onTouchTap={() => this.menuOpen()}><NavigationMenu/></IconButton>}
              />
              <Drawer
                  width={240}
                  docked={false}
                  open={openMenu}
                  containerStyle={{backgroundColor: grey800}}
                  onRequestChange={() => this.menuClose()}
              >
                <AppBar
                   title="Меню"
                   titleStyle={{color: 'black'}}
                   style={{backgroundColor: 'white'}}
                   iconElementLeft={<IconButton onTouchTap={() => this.menuClose()}><NavigationCloseIcon color="black"/></IconButton>}
                />
                <MenuContent
                  menu
                  setInfoPage={this.props.setInfoPage}
                  setMenuType={this.props.setMenuType}
                  isOpenMenu={this.props.isOpenMenu}
                  infoPage={this.props.infoPage}
                  setTitle={this.props.setTitle}
                  pushState={this.props.pushState}
                  menuClose={this.menuClose}
                />
              </Drawer>
              <Helmet {...config.app.head}/>
            </div>
            <div className="hide-on-small-only">
              <MenuContent
                menu={false}
                setInfoPage={this.props.setInfoPage}
                setMenuType={this.props.setMenuType}
                isOpenMenu={this.props.isOpenMenu}
                infoPage={this.props.infoPage}
                setTitle={this.props.setTitle}
                pushState={this.props.pushState}
                menuClose={this.menuClose}
              />
            </div>
            <div className={'col s12 ' + styles.content + ' ' + classContent} style={{marginTop: '-20px'}}>
              <div className="row">
                <div className="col s12">
                  {this.props.children}
                </div>
              </div>
            </div>
          </div>
       </MuiThemeProvider>
    );
  }
}
