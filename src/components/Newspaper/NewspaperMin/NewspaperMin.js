import React from 'react';

import {Link} from 'react-router';

import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {white, teal300} from 'material-ui/styles/colors';

export default class NewspaperMin extends React.Component {
  static propTypes = {
    // redux
    pushState: React.PropTypes.func.isRequired,
  };
  render() {
    return (
      <div className="row">
        <RaisedButton label="Создать новый номер" backgroundColor={teal300}/>
        <Table
          selectable={false}
        >
          <TableHeader
            displaySelectAll={false}
          >
            <TableRow>
              <TableHeaderColumn>№</TableHeaderColumn>
              <TableHeaderColumn>Дата</TableHeaderColumn>
              <TableHeaderColumn>Выкладывал</TableHeaderColumn>
              <TableHeaderColumn>Статус</TableHeaderColumn>
              <TableHeaderColumn/>
              <TableHeaderColumn/>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
          >
            <TableRow>
              <TableRowColumn><Link to="/newspaper/page12321">12321</Link></TableRowColumn>
              <TableRowColumn>asdasd</TableRowColumn>
              <TableRowColumn>asdsad</TableRowColumn>
              <TableRowColumn>asdasd</TableRowColumn>
              <TableRowColumn><RaisedButton label="Изменить" backgroundColor={teal300} /></TableRowColumn>
              <TableRowColumn><RaisedButton label="Удалить" secondary labelColor={white} rippleStyle={{backgroundColor: ''}}/></TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
        <FlatButton onTouchTap={() => this.props.pushState('/newspaper')} label="Перейти ко всем газетам" style={{width: '100%', marginTop: '20px'}}/>
      </div>
    );
  }
}
