import React from 'react';

import FlatButton from 'material-ui/FlatButton';
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar';

export default class RightMenuNavbar extends React.Component {
  render() {
    return (
      <Toolbar className="hide-on-small-only" style={{background: 'transparent', marginTop: '-5px'}}>
        <ToolbarGroup>
          <FlatButton style={{margin: '0 0'}} href="http://vechorka.ru" label="Вурнуться на сайт"/>
          <FlatButton style={{margin: '0 0'}} label="Выход"/>
        </ToolbarGroup>
      </Toolbar>
    );
  }
}
