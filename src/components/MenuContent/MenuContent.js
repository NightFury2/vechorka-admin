import React from 'react';

import MenuItem from './MenuItem/MenuItem';

import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';
import {ListItem} from 'material-ui/List';
import {grey500} from 'material-ui/styles/colors';

import HomeIcon from 'material-ui/svg-icons/action/home';
import AccountBox from 'material-ui/svg-icons/action/account-box';
import TextFormat from 'material-ui/svg-icons/content/text-format';
import ArtTrack from 'material-ui/svg-icons/av/art-track';
import LibaryBooks from 'material-ui/svg-icons/av/library-books';
import List from 'material-ui/svg-icons/action/list';
import GraphicEq from 'material-ui/svg-icons/device/graphic-eq';
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import AccountCircle from 'material-ui/svg-icons/action/account-circle';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

const menuItem = [
  {
    id: 1,
    active: true,
    icon: <HomeIcon color={grey500}/>,
    label: 'Главная',
    link: '/'
  },
  {
    id: 2,
    active: false,
    icon: <AccountBox color={grey500}/>,
    label: 'Личное',
    link: '/personal'
  },
  {
    id: 3,
    active: false,
    icon: <TextFormat color={grey500}/>,
    label: 'Коррекатура',
    link: '/proofreading'
  },
  {
    id: 4,
    active: false,
    icon: <ArtTrack color={grey500}/>,
    label: 'Газета',
    link: '/newspaper'
  },
  {
    id: 5,
    active: false,
    icon: <LibaryBooks color={grey500}/>,
    label: 'Новости',
    link: '/news'
  },
  {
    id: 6,
    active: false,
    icon: <List color={grey500}/>,
    label: 'Списки',
    link: '/lists'
  },
  {
    id: 7,
    active: false,
    icon: <GraphicEq color={grey500}/>,
    label: 'Статистика',
    link: '/statistics'
  }
];

export default class MenuContent extends React.Component {
  static propTypes = {
    menu: React.PropTypes.bool,
    //
    pushState: React.PropTypes.func,
    menuClose: React.PropTypes.func.isRequired,
    setTitle: React.PropTypes.func.isRequired,
    // global
    infoPage: React.PropTypes.object.isRequired,
    isOpenMenu: React.PropTypes.bool.isRequired,
    setInfoPage: React.PropTypes.func.isRequired,
    setMenuType: React.PropTypes.func.isRequired
  };
  render() {
    const styles = require('./MenuContent.scss');
    const iconHideMenuButton = this.props.isOpenMenu ? <ChevronLeft color="white"/> : <ChevronRight color="white"/>;
    const tooltipHideMenuButton = this.props.isOpenMenu ? 'Скрыть меню' : 'Показать меню';
    const smallMenuContainer = this.props.isOpenMenu ? '' : styles.menuContainerSmall;
    const smallMenuAvatar = !this.props.isOpenMenu ? styles.menuSmallAvatar : '';
    return (
       <div className={styles.menuContainer + ' ' + smallMenuContainer}>
         <div className="row">
           <ListItem leftAvatar={<Avatar icon={<AccountCircle color="black"/>}/>}
                     disabled
                     className={styles.menuAvatar + ' ' + smallMenuAvatar}
           >
             {this.props.isOpenMenu &&
              <span>
                name
              </span>
             }
           </ListItem>
         </div>
         {menuItem.map(item => {
           if (this.props.infoPage.id === item.id) {
             item.active = true;
           } else {
             item.active = false;
           }
           return item;
         }).map((item) => {
           return (
             <MenuItem isActive={item.active}
                       link={item.link}
                       labels={this.props.isOpenMenu ? item.label : ''}
                       isSmall={!this.props.isOpenMenu}
                       pushState={this.props.pushState}
                       {...item}
                       key={item.id}
             />
           );
         })}
         {!this.props.menu &&
           <IconButton className={styles.menuHideButton}
                       children={iconHideMenuButton}
                       tooltip={tooltipHideMenuButton}
                       tooltipPosition="bottom-right"
                       onTouchTap={() => this.props.setMenuType(!this.props.isOpenMenu)}
           />
         }
       </div>
    );
  }
}
