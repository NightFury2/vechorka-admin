import React from 'react';

import FlatButton from 'material-ui/FlatButton';
import {grey300, grey500} from 'material-ui/styles/colors';

export default class MenuItem extends React.Component {
  render() {
    const item = this.props;
    const styles = require('./MenuItem.scss');
    const activeClass = item.isActive ? styles.menuItemActive : '';
    const activeColor = item.isActive ? 'white' : grey500;
    const smallClass = item.isSmall ? styles.smallMenuItem : '';
    return (
      <FlatButton labelStyle={{color: activeColor}}
                  className={styles.menuItem + ' ' + activeClass + ' ' + smallClass}
                  label={item.labels}
                  icon={item.icon}
                  onTouchTap={() => item.pushState(item.link)}
                  hoverColor={grey300}
       />
    );
  }
}
