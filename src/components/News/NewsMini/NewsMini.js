import React from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import {Card, CardHeader, CardText, CardActions} from 'material-ui/Card';
import {blueGrey600, white, teal300} from 'material-ui/styles/colors';

export default class NewsMini extends React.Component {
  static propTypes = {
    // redux
    pushState: React.PropTypes.func.isRequired,
  };
  render() {
    return (
      <div className="row">
        <RaisedButton label="Добавить новсть" backgroundColor={teal300}/>
        <Card style={{marginTop: '20px'}}>
          <CardHeader
            title="title"
            subtitle="data"
            showExpandableButton
          />
          <Divider/>
          <CardText expandable>
            asdsadsadasdasd
          </CardText>
          <CardActions expandable style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'row'}}>
            <RaisedButton label="Удалить" secondary />
            <RaisedButton label="Отключить" backgroundColor={teal300}/>
          </CardActions>
          <Divider/>
          <CardText style={{backgroundColor: blueGrey600}} color={white}>
            Рубрика: ффывфывфывфв
            кластер, смена СКФО, КРСК
          </CardText>
        </Card>
        <FlatButton onTouchTap={() => this.props.pushState('/news')} label="Перейти ко всем новостям" style={{width: '100%', marginTop: '20px'}}/>
      </div>
    );
  }
}
